import { Component, OnInit } from '@angular/core';
import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { ChartType } from 'chart.js';
import { Label, MultiDataSet } from 'ng2-charts';

@Component({
  selector: 'app-project-chart',
  templateUrl: './projects-chart.component.html',
})
export class ProjectChartComponent implements OnInit {
  public doughnutChartLabels: Label[] = [];
  public doughnutChartData: MultiDataSet = [];

  public donutColors = [
    {
      backgroundColor: [
        '#F4D03F',
        '#52BE80',
        '#5499C7',
        '#BB8FCE',
        '#F1948A',
        '#F5B041',
        '#D7DBDD',
        '#85929E',
        '#BB909A',
        '#F4D00F',
        '#3479C6',
      ],
    },
  ];

  public doughnutChartType: ChartType = 'doughnut';

  constructor(
    public readonly firebaseService: FirebaseService,
  ) { }

  public ngOnInit(): void {
    this.labelsForChart();
    this.dataForChart();
  }

  public labelsForChart(): void {
    this.firebaseService.firebaseEmployeeProjects()
      .subscribe(response => {
        const names = response;
        for (let i = 0; i < names.length; i++) {
          const arr = names[i];
          this.doughnutChartLabels.push(arr.name);
        }
      });
  }

  public dataForChart(): void {
    this.firebaseService.getUsersForProjects()
      .subscribe(response => {
        const users = response;
        const LGT = users.filter(item => item.project === 'LGT').length;
        const Apex = users.filter(item => item.project === 'Apex').length;
        const Sinapsis = users.filter(item => item.project === 'Sinapsis').length;
        const Foodkit = users.filter(item => item.project === 'Foodkit').length;
        const Foodmasto = users.filter(item => item.project === 'Foodmaslo').length;
        const BRb = users.filter(item => item.project === 'BRb').length;
        const Pustoy = users.filter(item => item.project === 'Пустой').length;
        this.doughnutChartData.push(Apex, LGT, Sinapsis, Foodkit, Foodmasto, BRb, Pustoy);
      });
  }
}
