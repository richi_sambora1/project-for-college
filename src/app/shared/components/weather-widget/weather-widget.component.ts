import { Component, OnInit } from '@angular/core';

import { WeatherRequestService } from '@services/http-requests/weather-request/weather-request.service';

@Component({
  selector: 'weather-widget',
  templateUrl: './weather-widget.component.html',
  styleUrls: ['./weather-widget.component.scss'],
})
export class WeatherWidgetComponent implements OnInit {
  public weatherLocation: string;
  public weatherWind: string;
  public weatherTemp: string;
  public weatherTempFillsLike: string;

  constructor(
    public readonly weatherRequestService: WeatherRequestService,
  ) { }

  public ngOnInit(): void {
    this.weatherRequestService.getWeatherRequest$()
      .subscribe(
        response => {
          this.weatherLocation = (response.name);
          this.weatherWind = JSON.stringify(response.wind['speed']);
          this.weatherTemp = JSON.stringify(response.main['temp']);
          this.weatherTempFillsLike = JSON.stringify(response.main['feels_like']);
        },
      );
  }
}
