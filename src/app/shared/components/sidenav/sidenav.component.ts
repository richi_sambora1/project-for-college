import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent {
  public isCollapsed: boolean;

  constructor(
    private readonly router: Router,
  ) { }

  public goToLogin(): void {
    this.router.navigate(['/login']);
  }
}
