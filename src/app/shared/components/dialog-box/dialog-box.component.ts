import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { EmployeePositions } from '@shared/components/models/interfaces/employeePositions';
import { EmployeeProjects } from '@shared/components/models/interfaces/employeeProjects';
import { AngularFireDatabase } from 'angularfire2/database';

import { Employee } from '@shared/components/models/interfaces/employee';

@Component({
  selector: 'dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss'],
})
export class DialogBoxComponent implements OnInit {
  public localData: any;
  public show = false;
  public positionsList: Array<EmployeePositions>;
  public projectList: Array<EmployeeProjects>;

  public minDateWorkSince = '2015-01-01';
  public maxDateWorkSince = '2020-03-06';
  public minDateBirth = '1970-01-01';
  public maxDateBirth = '2002-01-01';

  constructor(
    public readonly db: AngularFireDatabase,
    public readonly firebaseService: FirebaseService,
    public readonly dialogRef: MatDialogRef<DialogBoxComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Employee) {
    this.localData = {...data};
  }

  public ngOnInit(): void {
    this.firebaseService.getPositions()
      .subscribe(response => {
        this.positionsList = response;
      });
    this.firebaseService.getProjects()
      .subscribe(response => {
        this.projectList = response;
      });
  }

  public closeDialog(): void {
    this.dialogRef.close({event: 'Cancel'});
  }

  public onSubmit(): void {
    if (this.firebaseService.form.valid) {
      if (this.firebaseService.form.get('$key').value == null)
        this.firebaseService.addUsers(this.firebaseService.form.value);
        else
          this.firebaseService.updateUser(this.firebaseService.form.value);
      this.firebaseService.form.reset();
    }
  }

  public passwordShow(): void {
    this.show = !this.show;
  }
}
