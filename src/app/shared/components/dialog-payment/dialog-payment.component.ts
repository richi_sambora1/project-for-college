import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { Employee } from '@shared/components/models/interfaces/employee';

@Component({
  selector: 'dialog-payment',
  templateUrl: './dialog-payment.component.html',
  styleUrls: ['./dialog-payment.component.scss'],
})
export class DialogPaymentComponent implements OnInit {
  public currentData: string;

  constructor(
    public readonly firebaseService: FirebaseService,
    public dialogRef: MatDialogRef<DialogPaymentComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Employee,
  ) { }

  public ngOnInit(): void {
    this.getCurrentDate();
  }

  public closeDialog(): void {
    this.dialogRef.close({event: 'Cancel'});
  }

  public exportToWordDoc(): void {
    const preHtml = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40"><head><meta charset="utf-8"><title>Export HTML To Doc</title></head><body>';
    const postHtml = '</body></html>';
    const html = preHtml + document.getElementById('word-table').innerHTML + postHtml;

    const blob = new Blob(['\ufeff', html], {
      type: 'application/msword',
    });

    const url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);
    const filename = 'Расчётный лист.doc';
    const downloadLink = document.createElement('a');
    document.body.appendChild(downloadLink);
    if (navigator.msSaveOrOpenBlob) {
      navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      downloadLink.href = url;
      downloadLink.download = filename;
      downloadLink.click();
    }

    document.body.removeChild(downloadLink);
  }

  public getCurrentDate(): void {
    const today = new Date();
    const day = String(today.getDate()).padStart(2, '0');
    const month = String(today.getMonth() + 1).padStart(2, '0');
    const year = today.getFullYear();
    this.currentData = day + '/' + month + '/' + year;
  }

}
