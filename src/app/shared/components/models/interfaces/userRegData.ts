export class UserRegData {
  public email: string;
  public password: string;
  public name: string;
  public position: string;
  public dateOfBirth: string;
}
