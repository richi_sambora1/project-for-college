export class Employee {
  id: string;
  name: string;
  status: string;
  project: string;
  position: string;
  workSince: string;
  workDays: number;
  salaryPerHour: number;
  dateOfBirth: string;
  email: string;
  password: string;
}
