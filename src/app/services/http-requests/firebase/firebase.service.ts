import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EmployeePositions } from '@shared/components/models/interfaces/employeePositions';
import { EmployeeProjects } from '@shared/components/models/interfaces/employeeProjects';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { Employee } from '@shared/components/models/interfaces/employee';
import { MonthDates } from '@shared/components/models/interfaces/monthDates';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  public employeesCount: number;
  public workDays: number;
  public salaryRate: number;
  public salaryPerMonth: number;
  public salaryWithBonus: number;
  public salaryVacations: number;
  public salaryPoDohod: number;
  public salaryBGS: number;
  public salaryPensiya: number;
  public salaryFSZN: number;
  public userPosition: string;
  public userName: string;
  public allMinuses: number;
  public salaryAvans: number;
  public totalWorkHours: number;
  public salaryProfsouz: number;
  public salaryItog: number;
  public salaryVicheti: number;
  public salaryToBank: number;
  public salaryBolnichni: number;
  public salaryPerDay: number;
  public dayOfVacations = 25;
  public disease: number;
  public komandir: number;
  public itogo: number;
  public salaryKomandir: number;

  public employees: Array<Employee>;
  public usersList: AngularFireList<Employee>;
  public regUsersList: AngularFireList<Employee>;

  public monthListRefDecember: AngularFireList<MonthDates>;
  public monthListRefNovember: AngularFireList<MonthDates>;
  public monthListRefOctober: AngularFireList<MonthDates>;
  public monthListRefSeptember: AngularFireList<MonthDates>;
  public monthListRefAgust: AngularFireList<MonthDates>;
  public monthListRefJuly: AngularFireList<MonthDates>;
  public monthListRefJune: AngularFireList<MonthDates>;
  public monthListRefMay: AngularFireList<MonthDates>;
  public monthListRefApril: AngularFireList<MonthDates>;
  public monthListRefMart: AngularFireList<MonthDates>;
  public monthListRefFev: AngularFireList<MonthDates>;
  public monthListRefJan: AngularFireList<MonthDates>;
  public monthListRefDecemberOf19: AngularFireList<MonthDates>;
  public monthListRefNovemberOf19: AngularFireList<MonthDates>;
  public monthListRefOctoberOf19: AngularFireList<MonthDates>;
  public monthListRefSeptemberOf19: AngularFireList<MonthDates>;
  public monthListRefAgustOf19: AngularFireList<MonthDates>;
  public monthListRefJulyOf19: AngularFireList<MonthDates>;

  public positionsList: AngularFireList<EmployeePositions>;
  public projectList: AngularFireList<EmployeeProjects>;

  public statuses = [
    {name: 'Работает'},
    {name: 'Уволен'},
  ];

  formToReg = new FormGroup({
    $key: new FormControl(null),
    id: new FormControl('1'),
    name: new FormControl(''),
    status: new FormControl('Активный'),
    project: new FormControl('Нет проекта'),
    position: new FormControl(null),
    workSince: new FormControl('2019-09-09'),
    workDays: new FormControl('000'),
    email: new FormControl(''),
    password: new FormControl(''),
    salaryPerHour: new FormControl('000'),
    dateOfBirth: new FormControl(''),
  });

  form = new FormGroup({
    $key: new FormControl(null),
    id: new FormControl(''),
    name: new FormControl(''),
    status: new FormControl(''),
    project: new FormControl(''),
    position: new FormControl(null),
    workSince: new FormControl(''),
    workDays: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
    salaryPerHour: new FormControl(''),
    dateOfBirth: new FormControl(''),
  });

  formUpdate = new FormGroup({
    $key: new FormControl(null),
    workDays: new FormControl(''),
    businessTrip: new FormControl(''),
    compensatoryTime: new FormControl(''),
    mainDate: new FormControl(''),
    disease: new FormControl(''),
    date01: new FormControl(''),
    date02: new FormControl(''),
    date03: new FormControl(''),
    date04: new FormControl(''),
    date05: new FormControl(''),
    date06: new FormControl(''),
    date07: new FormControl(''),
    date08: new FormControl(''),
    date09: new FormControl(''),
    date10: new FormControl(''),
    date11: new FormControl(''),
    date12: new FormControl(''),
    date13: new FormControl(''),
    date14: new FormControl(''),
    date15: new FormControl(''),
    date16: new FormControl(''),
    date17: new FormControl(''),
    date18: new FormControl(''),
    date19: new FormControl(''),
    date20: new FormControl(''),
    date21: new FormControl(''),
    date22: new FormControl(''),
    date23: new FormControl(''),
    date24: new FormControl(''),
    date25: new FormControl(''),
    date26: new FormControl(''),
    date27: new FormControl(''),
    date28: new FormControl(''),
    date29: new FormControl(''),
    date30: new FormControl(''),
    weekend: new FormControl(''),
    name: new FormControl(''),
    position: new FormControl(''),
    totalHours: new FormControl(''),
    salaryPerHour: new FormControl(''),
  });

  formPositions = new FormGroup({
    $key: new FormControl(null),
    name: new FormControl(null),
  });

  formProjects = new FormGroup({
    $key: new FormControl(null),
    name: new FormControl(null),
  });

  constructor(
    public readonly db: AngularFireDatabase,
  ) { }

  public firebaseData(): void {
    const employee = this.db.list<Employee>('/workspaceUsers')
      .valueChanges()
      .subscribe(response => {
        this.employees = response;
        this.employeesCount = response.length;
      });
  }

  public getPositions(): Observable<any> {
    return this.db.list('/employeePositions').valueChanges();
  }

  public getProjects(): Observable<any> {
    return this.db.list('/employeeProjects').valueChanges();
  }

  public addNewEmployeePosition(position): void {
    this.positionsList.push({
      name: position.name,
    });
  }

  public firebaseEmployeePositions(): Observable<any> {
    this.positionsList = this.db.list('/employeePositions');

    return this.positionsList.valueChanges();
  }

  public firebaseEmployeeProjects(): Observable<any> {
    this.projectList = this.db.list('/employeeProjects');

    return this.projectList.valueChanges();
  }

  public addNewEmployeeProjects(project): void {
    this.projectList.push({
      name: project.name,
    });
  }

  public getUsers(): Observable<any> {
    this.usersList = this.db.list('/workspaceUsers');

    return this.usersList.snapshotChanges();
  }

  public getUsersForProjects(): Observable<any> {
    this.usersList = this.db.list('/workspaceUsers');

    return this.usersList.valueChanges();
  }

  public getRegUsers(): Observable<any> {
    this.regUsersList = this.db.list('/workspaceUsers');

    return this.regUsersList.snapshotChanges();
  }

  public addUsers(user): void {
    this.usersList.push({
      id: user.id,
      name: user.name,
      status: user.status,
      email: user.email,
      password: user.password,
      project: user.project,
      position: user.position,
      workSince: user.workSince,
      workDays: user.workDays,
      salaryPerHour: user.salaryPerHour,
      dateOfBirth: user.dateOfBirth,
    });
  }

  public regNewUser(user): void {
    this.regUsersList.push({
      id: user.id,
      name: user.name,
      status: user.status,
      email: user.email,
      password: user.password,
      project: user.project,
      position: user.position,
      workSince: user.workSince,
      workDays: user.workDays,
      salaryPerHour: user.salaryPerHour,
      dateOfBirth: user.dateOfBirth,
    });
  }

  public updateUser(user): void {
    this.usersList.update(user.$key,
      {
        id: user.id,
        name: user.name,
        status: user.status,
        email: user.email,
        password: user.password,
        project: user.project,
        position: user.position,
        workSince: user.workSince,
        workDays: user.workDays,
        salaryPerHour: user.salaryPerHour,
        dateOfBirth: user.dateOfBirth,
      });
  }

  /*methods for month*/
  public getMonthRefDecemberOf19(): Observable<any> {
    this.monthListRefDecemberOf19 = this.db.list('monthDates/01-12-2019');

    return this.monthListRefDecemberOf19.snapshotChanges();
  }

  public getMonthRefNovemberOf19(): Observable<any> {
    this.monthListRefNovemberOf19 = this.db.list('monthDates/01-11-2019');

    return this.monthListRefNovemberOf19.snapshotChanges();
  }

  public getMonthRefOctoberOf19(): Observable<any> {
    this.monthListRefOctoberOf19 = this.db.list('monthDates/01-10-2019');

    return this.monthListRefOctoberOf19.snapshotChanges();
  }

  public getMonthRefSeptemberOf19(): Observable<any> {
    this.monthListRefSeptemberOf19 = this.db.list('monthDates/01-09-2019');

    return this.monthListRefSeptemberOf19.snapshotChanges();
  }

  public getMonthRefAgustOf19(): Observable<any> {
    this.monthListRefAgustOf19 = this.db.list('monthDates/01-08-2019');

    return this.monthListRefAgustOf19.snapshotChanges();
  }

  public getMonthRefJulyOf19(): Observable<any> {
    this.monthListRefJulyOf19 = this.db.list('monthDates/01-07-2019');

    return this.monthListRefJulyOf19.snapshotChanges();
  }

  public getMonthRefDecember(): Observable<any> {
    this.monthListRefDecember = this.db.list('monthDates/01-12-2020');

    return this.monthListRefDecember.snapshotChanges();
  }

  public getMonthRefNovember(): Observable<any> {
    this.monthListRefNovember = this.db.list('monthDates/01-11-2020');

    return this.monthListRefNovember.snapshotChanges();
  }

  public getMonthRefOctober(): Observable<any> {
    this.monthListRefOctober = this.db.list('monthDates/01-10-2020');

    return this.monthListRefOctober.snapshotChanges();
  }

  public getMonthRefSeptember(): Observable<any> {
    this.monthListRefSeptember = this.db.list('monthDates/01-09-2020');

    return this.monthListRefSeptember.snapshotChanges();
  }

  public getMonthRefAgust(): Observable<any> {
    this.monthListRefAgust = this.db.list('monthDates/01-08-2020');

    return this.monthListRefAgust.snapshotChanges();
  }

  public getMonthRefJuly(): Observable<any> {
    this.monthListRefJuly = this.db.list('monthDates/01-07-2020');

    return this.monthListRefJuly.snapshotChanges();
  }

  public getMonthRefJune(): Observable<any> {
    this.monthListRefJune = this.db.list('monthDates/01-06-2020');

    return this.monthListRefJune.snapshotChanges();
  }

  public getMonthRefMay(): Observable<any> {
    this.monthListRefMay = this.db.list('monthDates/01-05-2020');

    return this.monthListRefMay.snapshotChanges();
  }

  public getMonthRefApril(): Observable<any> {
    this.monthListRefApril = this.db.list('monthDates/01-04-2020');

    return this.monthListRefApril.snapshotChanges();
  }

  public getMonthRefMart(): Observable<any> {
    this.monthListRefMart = this.db.list('monthDates/01-03-2020');

    return this.monthListRefMart.snapshotChanges();
  }

  public getMonthRefFev(): Observable<any> {
    this.monthListRefFev = this.db.list('monthDates/01-02-2020');

    return this.monthListRefFev.snapshotChanges();
  }

  public getMonthRefJan(): Observable<any> {
    this.monthListRefJan = this.db.list('monthDates/01-01-2020');

    return this.monthListRefJan.snapshotChanges();
  }

  /*update data for month*/
  public updateUserDecemberOf19(user): void {
    this.monthListRefDecemberOf19.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserNovemberOf19(user): void {
    this.monthListRefNovemberOf19.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserOctoberOf19(user): void {
    this.monthListRefOctoberOf19.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserSeptemberOf19(user): void {
    this.monthListRefSeptemberOf19.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserAgustOf19(user): void {
    this.monthListRefAgustOf19.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserJulyOf19(user): void {
    this.monthListRefJulyOf19.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserDecember(user): void {
    this.monthListRefDecember.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserNovember(user): void {
    this.monthListRefNovember.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserOctober(user): void {
    this.monthListRefOctober.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserSeptember(user): void {
    this.monthListRefSeptember.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserAgust(user): void {
    this.monthListRefAgust.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserJuly(user): void {
    this.monthListRefJuly.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserJune(user): void {
    this.monthListRefJune.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserMay(user): void {
    this.monthListRefMay.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserApril(user): void {
    this.monthListRefApril.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserMart(user): void {
    this.monthListRefMart.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserFev(user): void {
    this.monthListRefFev.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public updateUserJan(user): void {
    this.monthListRefJan.update(user.$key,
      {
        workDays: user.workDays,
        businessTrip: user.businessTrip,
        compensatoryTime: user.compensatoryTime,
        mainDate: user.mainDate,
        disease: user.disease,
        date01: user.date01,
        date02: user.date02,
        date03: user.date03,
        date04: user.date04,
        date05: user.date05,
        date06: user.date06,
        date07: user.date07,
        date08: user.date08,
        date09: user.date09,
        date10: user.date10,
        date11: user.date11,
        date12: user.date12,
        date13: user.date13,
        date14: user.date14,
        date15: user.date15,
        date16: user.date16,
        date17: user.date17,
        date18: user.date18,
        date19: user.date19,
        date20: user.date20,
        date21: user.date21,
        date22: user.date22,
        date23: user.date23,
        date24: user.date24,
        date25: user.date25,
        date26: user.date26,
        date27: user.date27,
        date28: user.date28,
        date29: user.date29,
        date30: user.date30,
        weekend: user.weekend,
        name: user.name,
        position: user.name,
        totalHours: user.totalHours,
        salaryPerHour: user.salaryPerHour,
      });
  }

  public deleteUser($key: string): void {
    this.usersList.remove($key);
  }

  public populateForm(user): void {
    this.form.setValue(user);
  }

  public populateFormUpdate(user): void {
    this.formUpdate.setValue(user);
  }

  public getAllSalary(item): void {
    this.salaryPerMonth = ((item.workDays * 8) * item.salaryPerHour);
    this.salaryVacations = Math.round(this.salaryPerMonth * 12 / 12 / 29.7 * this.dayOfVacations);
    this.salaryWithBonus = this.salaryPerMonth * 5 / 100;
    this.salaryPoDohod = this.salaryPerMonth * 13 / 100;
    this.salaryFSZN = (item.workDays * 8) * item.salaryPerHour / 100;
    this.salaryPensiya = (item.workDays * 8) * item.salaryPerHour / 100;
    this.salaryBGS = Math.round((item.workDays * 8) * item.salaryPerHour * 0.6 / 100);
    this.allMinuses = Math.round(this.salaryPoDohod + this.salaryPensiya + this.salaryFSZN + this.salaryBGS);
    this.salaryAvans = this.salaryPerMonth * 40 / 100;
    this.salaryProfsouz = (item.workDays * 8) * item.salaryPerHour / 100;
    this.salaryItog = this.salaryPerMonth + this.salaryWithBonus;
    this.salaryVicheti = Math.round(this.salaryProfsouz + this.salaryPoDohod + this.salaryPensiya);
    this.salaryToBank = Math.round(this.salaryItog - this.salaryVicheti);
    this.salaryPerDay = (this.salaryPerMonth * 6) / 182;
    this.salaryBolnichni = Math.round(this.salaryPerDay * 0.8 * 12);
    this.salaryRate = item.salaryPerHour;
    this.workDays = item.workDays;
    this.totalWorkHours = item.workDays * 8;
    this.userPosition = item.position;
    this.userName = item.name;
  }

  public getAllSalaryRef(item): void {
    this.workDays = item.workDays - item.disease - item.compensatoryTime - item.weekend - item.businessTrip;
    this.salaryPerMonth = ((this.workDays * 8) * item.salaryPerHour);
    this.salaryVacations = Math.round(this.salaryPerMonth * 12 / 12 / 29.7 * this.dayOfVacations);
    this.salaryWithBonus = this.salaryPerMonth * 5 / 100;
    this.salaryPoDohod = this.salaryPerMonth * 13 / 100;
    this.salaryFSZN = (item.workDays * 8) * item.salaryPerHour / 100;
    this.salaryPensiya = (item.workDays * 8) * item.salaryPerHour / 100;
    this.salaryBGS = Math.round((item.workDays * 8) * item.salaryPerHour * 0.6 / 100);
    this.allMinuses = Math.round(this.salaryPoDohod + this.salaryPensiya + this.salaryFSZN + this.salaryBGS);
    this.salaryAvans = this.salaryPerMonth * 40 / 100;
    this.salaryProfsouz = (item.workDays * 8) * item.salaryPerHour / 100;
    this.komandir = item.businessTrip;
    this.disease = item.disease;
    this.salaryKomandir = item.businessTrip * 9;
    this.salaryBolnichni = Math.round(this.salaryPerDay * 0.8 * item.disease);
    this.salaryPerDay = (this.salaryPerMonth * 6) / 182;
    this.salaryItog = this.salaryPerMonth + this.salaryWithBonus + this.salaryKomandir + this.salaryBolnichni;
    this.salaryVicheti = Math.round(this.salaryProfsouz + this.salaryPoDohod + this.salaryPensiya);
    this.salaryToBank = Math.round(this.salaryItog - this.salaryVicheti);
    this.salaryRate = item.salaryPerHour;
    this.totalWorkHours = item.workDays * 8;
    this.itogo = this.salaryToBank - this.salaryAvans;
    this.userPosition = item.position;
    this.userName = item.name;
  }
}
