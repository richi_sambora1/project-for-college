import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { WeatherRequestService } from '@services/http-requests/weather-request/weather-request.service';
import { UserDataService } from '@services/userData/user-data.service';

export const Services = [
  WeatherRequestService,
  FirebaseService,
  UserDataService,
];
