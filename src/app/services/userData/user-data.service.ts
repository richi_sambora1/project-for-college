import { Injectable } from '@angular/core';

import { UserRegData } from '@shared/components/models/interfaces/userRegData';
import { UserData } from '@shared/components/models/interfaces/userData';

@Injectable({
  providedIn: 'root',
})
export class UserDataService {
  public userData: UserData = new UserData();
  public userRegData: UserRegData = new UserRegData();
}
