import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { NgxSpinnerModule } from 'ngx-spinner';

import { Pages } from '@pages/index';
import { SharedComponents } from '@shared/components';
import { ProjectChartComponent } from '@shared/components/doughnut-chart/projects-chart.component';
import { MDBRootModule } from 'angular-bootstrap-md';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { ChartsModule } from 'ng2-charts';
import { environment } from '../environments/environment';
import { AppMaterialModule } from './app-material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Services } from './services';
import { DialogBoxComponent } from './shared/components/dialog-box/dialog-box.component';
import { DialogPaymentComponent } from './shared/components/dialog-payment/dialog-payment.component';

@NgModule({
  declarations: [
    AppComponent,
    Pages,
    SharedComponents,
    ProjectChartComponent,
    DialogBoxComponent,
    DialogPaymentComponent,
  ],
  imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFireDatabaseModule,
        ChartsModule,
        FormsModule,
        HttpClientModule,
        AppMaterialModule,
        AppRoutingModule,
        ChartsModule,
        MDBRootModule,
        ReactiveFormsModule,
        NgxSpinnerModule,
        MatSelectModule,
    ],
  entryComponents: [
    DialogBoxComponent,
  ],
  providers: [Services],
  bootstrap: [AppComponent],
})
export class AppModule {
}
