import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';

import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { DialogBoxComponent } from '@shared/components/dialog-box/dialog-box.component';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 's-time-track',
  templateUrl: './s-time-track.component.html',
  styleUrls: ['./s-time-track.component.scss'],
})
export class TimeTrackComponent {
  @Input() public usersArray: Array<any>;
  @Input() public sumOfSalary: number;
  @Input() public sumOfEmployee: number;
  @Input() public searchText: string;

  @Output() public getUsersCount: EventEmitter<void> = new EventEmitter();
  @Output() public getUsersSalary: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmit: EventEmitter<void> = new EventEmitter();

  @ViewChild(MatTable, {static: true}) table: MatTable<any>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public readonly firebaseService: FirebaseService,
    public readonly dialog: MatDialog,
    public readonly db: AngularFireDatabase,
  ) { }

  public openDialog(obj) {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '500px',
      height: '650px',
      data: obj,
    });
  }

  public onDelete($key): void {
    if (confirm('Вы уверены что хотите удалить эту запись?')) {
      this.firebaseService.deleteUser($key);
    }
  }

  public filterCondition(item) {
    return item.name.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
  }

  public sortTable1(n): void {
    let table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById('myTable');
    switching = true;
    dir = 'asc';
    while (switching) {
      switching = false;
      rows = table.rows;
      for (i = 1; i < (rows.length - 1); i++) {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName('TD')[n];
        y = rows[i + 1].getElementsByTagName('TD')[n];
        if (dir == 'asc') {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        } else if (dir == 'desc') {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount ++;
      } else {
        if (switchcount == 0 && dir == 'asc') {
          dir = 'desc';
          switching = true;
        }
      }
    }
  }

  public reload(): void {
    window.location.reload();
  }
}
