import { Sections } from '@pages/time-track/section';
import { ViewTimeTrackComponent } from '@pages/time-track/view/v-time-track.component';

export const TimeTrackComponents = [
  Sections,
  ViewTimeTrackComponent,
];
