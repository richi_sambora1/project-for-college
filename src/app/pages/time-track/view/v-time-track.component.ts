import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'time-track',
  templateUrl: './v-time-track.component.html',
})

export class ViewTimeTrackComponent implements OnInit {
  public usersArray = [];

  public sumOfSalary: number;
  public sumOfEmployee: number;
  public searchText = '';

  constructor(
    public readonly firebaseService: FirebaseService,
    public readonly dialog: MatDialog,
    public readonly db: AngularFireDatabase,
    private readonly spinner: NgxSpinnerService,
  ) { }

  public ngOnInit(): void {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 2000);
    this.firebaseService.getUsers()
      .subscribe(list => {
        this.usersArray = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
    this.firebaseService.firebaseData();
    this.firebaseService.firebaseEmployeePositions();
    this.firebaseService.firebaseEmployeeProjects();
    this.getUsersSalary();
    this.getUsersCount();
  }

  public getUsersCount(): void {
    this.sumOfEmployee = this.firebaseService.employeesCount;
  }

  public getUsersSalary(): void {
    this.firebaseService.getUsersForProjects()
      .subscribe(response => {
        const users = response;
        for (let i = 0; i < users.length; i++) {
          const arr = users[i];
          this.sumOfSalary = arr.salaryPerHour * (arr.workDays * 8);
        }
      });
  }

  public onSubmit(): void {
    this.firebaseService.addNewEmployeePosition(this.firebaseService.formPositions.value);
    this.firebaseService.formPositions.reset();

    this.firebaseService.addNewEmployeeProjects(this.firebaseService.formProjects.value);
    this.firebaseService.formProjects.reset();
  }
}
