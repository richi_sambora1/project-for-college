import { RegistrationComponent } from '@pages/registration/form/f-registration.component';
import { ViewRegistrationComponent } from '@pages/registration/view/v-registration.component';

export const RegistrationComponents = [
  ViewRegistrationComponent,
  RegistrationComponent,
];
