import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { UserDataService } from '@services/userData/user-data.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'f-registration',
  templateUrl: './f-registration.component.html',
  styleUrls: ['./f-registration.component.scss'],
})
export class RegistrationComponent {
  @Input() public userRegData: UserDataService;
  @Input() public usersArray: Array<any>;
  @Input() public positionsList: any;
  @Input() public show: boolean;

  @Output() public goToLogin: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmit: EventEmitter<void> = new EventEmitter();
  @Output() public passwordShow: EventEmitter<void> = new EventEmitter();

  constructor(
    public readonly router: Router,
    public readonly db: AngularFireDatabase,
    public readonly firebaseService: FirebaseService,
  ) { }
}
