import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { UserDataService } from '@services/userData/user-data.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'v-registration',
  templateUrl: './v-registration.component.html',
})
export class ViewRegistrationComponent implements OnInit {
  public usersArray = [];
  public show = false;
  public positionsList;

  constructor(
    public readonly router: Router,
    public readonly db: AngularFireDatabase,
    public readonly userDataService: UserDataService,
    public readonly firebaseService: FirebaseService,
  ) { }

  public ngOnInit(): void {
    this.firebaseService.getPositions()
      .subscribe(response => {
        this.positionsList = response;
      });
    this.firebaseService.getRegUsers()
      .subscribe(list => {
        this.usersArray = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public goToLogin(): void {
    this.router.navigate(['/login']);
  }

  public onSubmit(): void {
    if (this.firebaseService.formToReg.valid) {
      if (this.firebaseService.formToReg.get('$key').value == null)
        this.firebaseService.regNewUser(this.firebaseService.formToReg.value);
      this.firebaseService.formToReg.reset();
      this.router.navigate(['/login']);
    }
  }

  public passwordShow(): void {
    this.show = !this.show;
  }
}
