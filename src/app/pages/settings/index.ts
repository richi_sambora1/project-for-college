import { Settings } from '@pages/settings/section';
import { ViewSettingsComponent } from '@pages/settings/view/v-settings.component';

export const SettingsComponents = [
  Settings,
  ViewSettingsComponent,
];
