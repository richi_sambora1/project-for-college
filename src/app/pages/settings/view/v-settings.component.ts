import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';

@Component({
  selector: 'v-settings',
  templateUrl: './v-settings.component.html',
})
export class ViewSettingsComponent implements OnInit {
  public fileName = 'Ведомость.xlsx';

  public monthListRefDecember: string;
  public monthListRefNovember: string;
  public monthListRefOctober: string;
  public monthListRefSeptember: string;
  public monthListRefAgust: string;
  public monthListRefJuly: string;
  public monthListRefJune: string;
  public monthListRefMay: string;
  public monthListRefApril: string;
  public monthListRefMart: string;
  public monthListRefFev: string;
  public monthListRefJan: string;
  public monthListRefJulyOf19: string;
  public monthListRefDecemberOf19: string;
  public monthListRefNovemberOf19: string;
  public monthListRefOctoberOf19: string;
  public monthListRefSeptemberOf19: string;
  public monthListRefAgustOf19: string;
  public visibility: Boolean = false;

  constructor(
    public readonly firebaseService: FirebaseService,
    public readonly db: AngularFireDatabase,
    private readonly spinner: NgxSpinnerService,
    public readonly dialog: MatDialog,
  ) { }

  public ngOnInit(): void {
    this.firebaseService.firebaseData();
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 4000);
    setTimeout(() => {
      this.multiArray();
      this.multiArray2();
      this.multiArray3();
      this.multiArray4();
      this.multiArray5();
      this.multiArray6();
      this.multiArray7();
      this.multiArray8();
      this.multiArray9();
      this.multiArray10();
      this.multiArray11();
      this.multiArray12();
      this.multiArray13();
      this.multiArray14();
      this.multiArray15();
      this.multiArray16();
      this.multiArray17();
      this.multiArray18();
    }, 3000);
  }

  public exportExcelJan(): void {
    const element = document.getElementById('excel-table-jan');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelFev(): void {
    const element = document.getElementById('excel-table-fev');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelMart(): void {
    const element = document.getElementById('excel-table-mart');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelApril(): void {
    const element = document.getElementById('excel-table-april');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelMay(): void {
    const element = document.getElementById('excel-table-may');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelJune(): void {
    const element = document.getElementById('excel-table-june');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelJuly(): void {
    const element = document.getElementById('excel-table-july');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelAgust(): void {
    const element = document.getElementById('excel-table-agust');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelSeptember(): void {
    const element = document.getElementById('excel-table-september');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelOctober(): void {
    const element = document.getElementById('excel-table-october');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelNovember(): void {
    const element = document.getElementById('excel-table-november');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelDecember(): void {
    const element = document.getElementById('excel-table-december');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelDecemberOf19(): void {
    const element = document.getElementById('excel-table-decemberOf19');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelNovemberOf19(): void {
    const element = document.getElementById('excel-table-novemberOf19');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelOctoberOf19(): void {
    const element = document.getElementById('excel-table-octoberOf19');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelSetmeberOf19(): void {
    const element = document.getElementById('excel-table-septemberOf19');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelAgustOf19(): void {
    const element = document.getElementById('excel-table-agustOf19');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public exportExcelJulyOf19(): void {
    const element = document.getElementById('excel-table-julyOf19');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName);
  }

  public multiArray(): void {
    this.firebaseService.getMonthRefMay()
      .subscribe(list => {
        this.monthListRefMay = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray2(): void {
    this.firebaseService.getMonthRefJune()
      .subscribe(list => {
        this.monthListRefJune = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray3(): void {
    this.firebaseService.getMonthRefJuly()
      .subscribe(list => {
        this.monthListRefJuly = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray4(): void {
    this.firebaseService.getMonthRefAgust()
      .subscribe(list => {
        this.monthListRefAgust = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray5(): void {
    this.firebaseService.getMonthRefSeptember()
      .subscribe(list => {
        this.monthListRefSeptember = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray6(): void {
    this.firebaseService.getMonthRefOctober()
      .subscribe(list => {
        this.monthListRefOctober = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray7(): void {
    this.firebaseService.getMonthRefNovember()
      .subscribe(list => {
        this.monthListRefNovember = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray8(): void {
    this.firebaseService.getMonthRefDecember()
      .subscribe(list => {
        this.monthListRefDecember = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray9(): void {
    this.firebaseService.getMonthRefMart()
      .subscribe(list => {
        this.monthListRefMart = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray10(): void {
    this.firebaseService.getMonthRefFev()
      .subscribe(list => {
        this.monthListRefFev = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray11(): void {
    this.firebaseService.getMonthRefJan()
      .subscribe(list => {
        this.monthListRefJan = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray12(): void {
    this.firebaseService.getMonthRefApril()
      .subscribe(list => {
        this.monthListRefApril = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray13(): void {
    this.firebaseService.getMonthRefDecemberOf19()
      .subscribe(list => {
        this.monthListRefDecemberOf19 = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray14(): void {
    this.firebaseService.getMonthRefNovemberOf19()
      .subscribe(list => {
        this.monthListRefNovemberOf19 = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray15(): void {
    this.firebaseService.getMonthRefOctoberOf19()
      .subscribe(list => {
        this.monthListRefOctoberOf19 = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray16(): void {
    this.firebaseService.getMonthRefSeptemberOf19()
      .subscribe(list => {
        this.monthListRefSeptemberOf19 = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray17(): void {
    this.firebaseService.getMonthRefAgustOf19()
      .subscribe(list => {
        this.monthListRefAgustOf19 = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public multiArray18(): void {
    this.firebaseService.getMonthRefJulyOf19()
      .subscribe(list => {
        this.monthListRefJulyOf19 = list.map(item => {
          return {
            $key: item.key,
            ...item.payload.val(),
          };
        });
      });
  }

  public onSubmitDecember(): void {
    this.firebaseService.updateUserDecember(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitNovember(): void {
    this.firebaseService.updateUserNovember(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitOctober(): void {
    this.firebaseService.updateUserOctober(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitSeptember(): void {
    this.firebaseService.updateUserSeptember(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitAgust(): void {
    this.firebaseService.updateUserAgust(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitJuly(): void {
    this.firebaseService.updateUserJuly(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitJune(): void {
    this.firebaseService.updateUserJune(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitMay(): void {
    this.firebaseService.updateUserMay(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitApril(): void {
    this.firebaseService.updateUserApril(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitMart(): void {
    this.firebaseService.updateUserMart(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitFevral(): void {
    this.firebaseService.updateUserFev(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitJan(): void {
    this.firebaseService.updateUserJan(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitDecemberOf19(): void {
    this.firebaseService.updateUserDecemberOf19(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitNovemberOf19(): void {
    this.firebaseService.updateUserNovemberOf19(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitOctoberOf19(): void {
    this.firebaseService.updateUserOctoberOf19(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitSeptemberOf19(): void {
    this.firebaseService.updateUserSeptemberOf19(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitAgustOf19(): void {
    this.firebaseService.updateUserAgustOf19(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public onSubmitJulyOf19(): void {
    this.firebaseService.updateUserJulyOf19(this.firebaseService.formUpdate.value);
    this.firebaseService.formUpdate.reset();
  }

  public toggle(): void {
    this.visibility = !this.visibility;
  }
}
