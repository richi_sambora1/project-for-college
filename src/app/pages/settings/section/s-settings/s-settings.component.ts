import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { FirebaseService } from '@services/http-requests/firebase/firebase.service';
import { DialogPaymentComponent } from '@shared/components/dialog-payment/dialog-payment.component';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 's-settings',
  templateUrl: './s-settings.component.html',
  styleUrls: ['./s-settings.component.scss'],
})
export class SettingsComponent {
  public editRowID: any = '';

  @Input() public monthListRefDecember: any;
  @Input() public monthListRefNovember: any;
  @Input() public monthListRefOctober: any;
  @Input() public monthListRefSeptember: any;
  @Input() public monthListRefAgust: any;
  @Input() public monthListRefJuly: any;
  @Input() public monthListRefJune: any;
  @Input() public monthListRefMay: any;
  @Input() public monthListRefApril: any;
  @Input() public monthListRefMart: any;
  @Input() public monthListRefFev: any;
  @Input() public monthListRefJan: any;
  @Input() public monthListRefJulyOf19: any;
  @Input() public monthListRefDecemberOf19: any;
  @Input() public monthListRefNovemberOf19: any;
  @Input() public monthListRefOctoberOf19: any;
  @Input() public monthListRefSeptemberOf19: any;
  @Input() public monthListRefAgustOf19: any;
  @Input() public visibility: boolean;

  @Output() public exportExcelJan: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelFev: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelMart: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelApril: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelMay: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelJune: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelJuly: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelAgust: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelSeptember: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelOctober: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelNovember: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelDecember: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelDecemberOf19: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelNovemberOf19: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelOctoberOf19: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelSetmeberOf19: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelAgustOf19: EventEmitter<void> = new EventEmitter();
  @Output() public exportExcelJulyOf19: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray2: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray3: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray4: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray5: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray6: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray7: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray8: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray9: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray10: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray11: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray12: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray13: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray14: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray15: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray16: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray17: EventEmitter<void> = new EventEmitter();
  @Output() public multiArray18: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitDecember: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitNovember: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitOctober: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitSeptember: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitAgust: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitJuly: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitJune: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitMay: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitApril: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitMart: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitFevral: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitJan: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitDecemberOf19: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitNovemberOf19: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitOctoberOf19: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitSeptemberOf19: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitAgustOf19: EventEmitter<void> = new EventEmitter();
  @Output() public onSubmitJulyOf19: EventEmitter<void> = new EventEmitter();
  @Output() public toggle: EventEmitter<void> = new EventEmitter();

  constructor(
    public readonly firebaseService: FirebaseService,
    public readonly db: AngularFireDatabase,
    public readonly dialog: MatDialog,
  ) { }

  public openPaymentDialog(obj, item) {
    const dialogRef = this.dialog.open(DialogPaymentComponent, {
      width: '970px',
      height: '500px',
      data: this.firebaseService.getAllSalaryRef(item),
    });
  }
}
