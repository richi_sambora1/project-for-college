import { LoginComponents } from '@pages/login';
import { RegistrationComponents } from '@pages/registration';
import { SettingsComponents } from '@pages/settings';
import { TechnologiesComponents } from '@pages/technologies';
import { TimeTrackComponents } from '@pages/time-track';

export const Pages = [
  SettingsComponents,
  TimeTrackComponents,
  TechnologiesComponents,
  LoginComponents,
  RegistrationComponents,
];
