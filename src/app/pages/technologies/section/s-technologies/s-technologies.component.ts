import { Component } from '@angular/core';

@Component({
  selector: 's-technologies',
  templateUrl: './s-technologies.component.html',
  styleUrls: ['./s-technologies.component.scss'],
})
export class TechnologiesComponent {
  public panelOpenState = false;

}
