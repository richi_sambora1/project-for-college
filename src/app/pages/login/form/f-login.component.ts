import { Component, EventEmitter, Input, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserDataService } from '@services/userData/user-data.service';

@Component({
  selector: 'f-login',
  templateUrl: './f-login.component.html',
  styleUrls: ['./f-login.component.scss'],
})

export class LoginComponent {
  @Input() public userData: UserDataService;
  @Input() public emailReq: string;
  @Input() public passwordReq: string;
  @Input() public show: boolean;

  @Output() public passwordShow: EventEmitter<void> = new EventEmitter();
  @Output() public goToReg: EventEmitter<void> = new EventEmitter();
  @Output() public goToPage: EventEmitter<void> = new EventEmitter();

  constructor(
    public readonly router: Router,
  ) { }

  public goToProfile(form: NgForm): void {
    if (this.userData.userData.email === 'nikipusik1@mail.ru' && this.userData.userData.password === 'detroit313')
      this.router.navigate(['/userProfile']);
    else if (this.userData.userData.email === 'admin@mail.ru' && this.userData.userData.password === 'admin1234')
      this.router.navigate(['/time-track']);
    else alert('Неверный пароль или email');
    form.resetForm();
  }
}
