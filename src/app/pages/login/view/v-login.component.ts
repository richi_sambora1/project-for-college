import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserDataService } from '@services/userData/user-data.service';

@Component({
  selector: 'v-login',
  templateUrl: './v-login.component.html',
})
export class ViewLoginComponent {
  public show = false;
  public emailReq = 'nikipusik1@mail.ru';
  public passwordReq = 'detroit313';

  constructor(
    public readonly router: Router,
    public readonly userDataService: UserDataService,
  ) { }

  public goToReg(): void {
    this.router.navigate(['/registration']);
  }

  public passwordShow(): void {
    this.show = !this.show;
  }
}
