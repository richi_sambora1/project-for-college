import { LoginComponent } from '@pages/login/form/f-login.component';
import { ViewLoginComponent } from '@pages/login/view/v-login.component';

export const LoginComponents = [
  LoginComponent,
  ViewLoginComponent,
];
