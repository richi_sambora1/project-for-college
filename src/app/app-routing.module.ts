import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ViewLoginComponent } from '@pages/login/view/v-login.component';
import { ViewRegistrationComponent } from '@pages/registration/view/v-registration.component';
import { ViewSettingsComponent } from '@pages/settings/view/v-settings.component';
import { ViewTechnologiesComponent } from '@pages/technologies/view/v-technologies.component';
import { ViewTimeTrackComponent } from '@pages/time-track/view/v-time-track.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'registration', component: ViewRegistrationComponent },
  { path: 'login', component: ViewLoginComponent },
  { path: 'time-track', component: ViewTimeTrackComponent },
  { path: 'settings', component: ViewSettingsComponent },
  { path: 'technologies', component: ViewTechnologiesComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
